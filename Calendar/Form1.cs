﻿using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Threading;


namespace Calendar
{
    public partial class Form1 : Form
    {

        //private List<smena> smenaAll;
        private List<smena> smenaEFS;
        private List<smena> smenaODS;
        private List<taxi> taxiClass;

        private List<myEvent> allEvents;
        public List<string> allUsers;
        public OpenFileDialog filDia;
        public string filePath;
        public string dateStartJson, dateEndJson;
        private StringBuilder listTaxi;
        private List<DateTime> holydays;
        public int cntTaxi;


        public Form1()
        {
            taxiClass = new List<taxi>();
            smenaEFS = new List<smena>();
            smenaODS = new List<smena>();
            InitializeComponent();
            CheckForIllegalCrossThreadCalls = false;
            allUsers = new List<string>();
            allEvents = new List<myEvent>();
            filePath = "";
            dateStartJson = "";
            dateEndJson = "";
            doReadCalXML();
            listTaxi = new StringBuilder();
            label3.Visible = false;
        }


        private void button1_Click(object sender, EventArgs e)
        {
            filDia = new OpenFileDialog();
            filDia.Filter = "Файлы Excel(*.xlsx)|*.xlsx|Все файлы (*.*)|*.*";
            filDia.ShowDialog();

            if (filDia.FileName.ToString() != "")
            {
                textBox1.Text = filDia.FileName.ToString();
                filePath = filDia.FileName.ToString().Substring(0, filDia.FileName.ToString().LastIndexOf('.'));

                //textBox2.Text = filePath;
                label4.Text = "Загрузка файла... Подождите";
                label4.Visible = true;
                doFirstAnalys();
                label3.Visible = true;
                label4.Visible = false;
            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text.Trim() == "") MessageBox.Show("Вы не выбрали файл!");
            else
            {
                taxiClass = new List<taxi>();
                cntTaxi = 0;
                richTextBox1.Text = "";
                listTaxi = new StringBuilder();
                Thread doEvents = new Thread(makeEvents);
                doEvents.Start();
            }
        }

        private void doFirstAnalys()
        {

            int rowPeriod = 0;
            int colEndPeriod = 0;
            int startSmenaFS = 0;
            int endSmenaFS = 0;
            string nameSmenaFS = "";
            int startSmenaEFS = 0;
            int endSmenaEFS = 0;
            string nameSmenaEFS = "";
            int startSmenaODS = 0;
            int endSmenaODS = 0;
            string nameSmenaODS = "";
            Excel.Application excelApp = new Excel.Application();
            excelApp.Visible = false;

            excelApp.Workbooks.Open(textBox1.Text);

            Excel.Worksheet fstWSH = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];

            try
            {
                label3.Text = "" + excelApp.Cells[1, 1].Value;
                int rowEndGlobal = excelApp.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
                int colEndGlobal = 40;

                for (int i = 2; i < rowEndGlobal; i++)
                {
                    if (excelApp.Cells[i, 1].Value == "ЕФС")
                    {
                        nameSmenaEFS = excelApp.Cells[i, 1].Value;
                        label1.Text = " Выбранные подразделения:  " + nameSmenaEFS + ", ";
                        startSmenaEFS = i + 1;
                        endSmenaFS = i - 1;
                    }

                    if (excelApp.Cells[i, 1].Value == "ОДС")
                    {
                        nameSmenaODS = excelApp.Cells[i, 1].Value;
                        label1.Text += nameSmenaODS;
                        startSmenaODS = i + 1;
                        endSmenaEFS = i - 1;
                    }

                    if (excelApp.Cells[i, 1].Value == "END")
                    {
                        endSmenaODS = i - 2;
                        rowPeriod = i - 1;
                        break;
                    }

                }

                for (int i = 3; i < colEndGlobal; i++)
                {


                    if (excelApp.Cells[rowPeriod, i].Value == null)
                    {

                        colEndPeriod = i - 1;
                        break;
                    }
                    else if ((excelApp.Cells[rowPeriod, i].Value.ToString() == "Ф.И.О.") || (excelApp.Cells[rowPeriod, i].Interior.ColorIndex == -7))
                    {

                        colEndPeriod = i - 1;
                        break;
                    }


                }


                // Сводная инфа
                richTextBox1.Text = richTextBox1.Text + "Ячейки глобал : Ряд  " + rowEndGlobal + ", столбец " + colEndGlobal + "\n";
                
                richTextBox1.Text = richTextBox1.Text + "Сотрудники в смене EFS, строки с " + startSmenaEFS + " по " + endSmenaEFS + "\n";
                richTextBox1.Text = richTextBox1.Text + "Сотрудники в смене ODS, строки с " + startSmenaODS + " по " + endSmenaODS + "\n";
                richTextBox1.Text = richTextBox1.Text + "Ячейки периода : Ряд  " + rowPeriod + ", столбец " + colEndPeriod + "\n";
                richTextBox1.Text = richTextBox1.Text + "Дни в месяце : " + excelApp.Cells[rowPeriod, 3].Value + " - " + excelApp.Cells[rowPeriod, colEndPeriod].Value + "\n\n";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            excelApp.ActiveWorkbook.Close();
            excelApp.Quit();

        }

        private void makeEvents()
        {
            bool checkOK = false;
            button2.Enabled = false;

            int rowPeriod = 0;
            int colEndPeriod = 0;
            int startSmenaFS = 0;
            int endSmenaFS = 0;
            string nameSmenaFS = "";
            int startSmenaEFS = 0;
            int endSmenaEFS = 0;
            string nameSmenaEFS = "";
            int startSmenaODS = 0;
            int endSmenaODS = 0;
            string nameSmenaODS = "";
            int eventMonth = 0;
            int eventYear = 0;
            

            richTextBox1.Text = richTextBox1.Text + "Собираем сводную информацию...\n";

            Excel.Application excelApp = new Excel.Application();
            excelApp.Visible = false;

            excelApp.Workbooks.Open(textBox1.Text);

            Excel.Worksheet fstWSH = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];

            try
            {
                // Блок сбора общей информации
                eventMonth = int.Parse(excelApp.Cells[1, 1].Value.Split('.')[0]);
                eventYear = int.Parse(excelApp.Cells[1, 1].Value.Split('.')[1]);

                //                richTextBox1.Text = richTextBox1.Text + "Выбраный месяц : " + excelApp.Cells[1, 1].Value + "\n";



                int rowEndGlobal = excelApp.Cells.SpecialCells(Excel.XlCellType.xlCellTypeLastCell).Row;
                int colEndGlobal = 40;

                for (int i = 2; i < rowEndGlobal; i++)
                {
                    if (excelApp.Cells[i, 1].Value == "ЕФС")
                    {
                        nameSmenaEFS = excelApp.Cells[i, 1].Value;
                        label1.Text = " Выбранные подразделения:  " + nameSmenaEFS + ", ";
                        startSmenaEFS = i + 1;
                        endSmenaFS = i - 1;
                    }

                    if (excelApp.Cells[i, 1].Value == "ОДС")
                    {
                        nameSmenaODS = excelApp.Cells[i, 1].Value;
                        label1.Text += nameSmenaODS;
                        startSmenaODS = i + 1;
                        endSmenaEFS = i - 1;
                    }

                    if (excelApp.Cells[i, 1].Value == "END")
                    {
                        endSmenaODS = i - 2;
                        rowPeriod = i - 1;
                        break;
                    }

                }

                for (int i = 3; i < colEndGlobal; i++)
                {


                    if (excelApp.Cells[rowPeriod, i].Value == null)
                    {

                        colEndPeriod = i - 1;
                        break;
                    }
                    else if ((excelApp.Cells[rowPeriod, i].Value.ToString() == "Ф.И.О.") || (excelApp.Cells[rowPeriod, i].Interior.ColorIndex == -7))
                    {

                        colEndPeriod = i - 1;
                        break;
                    }


                }

                //MessageBox.Show("112");
                // "dateTime":"2016-10-01T00:00:00+10:00"
                // "dateTime":"2016-10-31T23:59:00+10:00"

                string text = excelApp.Cells[1, 1].Value;
                dateStartJson = text.Split('.')[1] + "-" + text.Split('.')[0] + "-0" + excelApp.Cells[rowPeriod, 3].Value + "T07:00:00+10:00";
                DateTime ddEndJson = new DateTime(int.Parse(text.Split('.')[1]), int.Parse(text.Split('.')[0]), +(int)excelApp.Cells[rowPeriod, colEndPeriod].Value);
                dateEndJson = "" + ddEndJson.Year + "-" + prevZero(ddEndJson.Month) + "-" + prevZero(ddEndJson.Day) + "T21:00:00+10:00";

                checkOK = true;

                /*
                График ЕФС
                0biiqso78lfgfj9blab8ps7vdc@group.calendar.google.com


                ОДС
                bairdkjis3dsjdbbg10e7edua0@group.calendar.google.com
                */

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

            if (checkOK)
            { 

                /*
                // Блок выгрузки АС ФС
                allUsers = new List<string>();
                allEvents = new List<myEvent>();
                try
                {
                    richTextBox1.Text = richTextBox1.Text + "Собираем график по АС ФС...\n";
                    int cnt1 = 0;
                    for (int i = startSmenaFS; i <= endSmenaFS; i++)
                    {

                        if (excelApp.Cells[i, 1].Value != null)
                        {

                            richTextBox1.Text = richTextBox1.Text + excelApp.Cells[i, 1].Value + "\n";
                            allUsers.Add(excelApp.Cells[i, 1].Value.Split(' ')[0].Split('-')[0]);
                            cnt1++;
                        }
                        else allUsers.Add("");

                    }
                    richTextBox1.Text = richTextBox1.Text + "Сотрудники в смене: " + cnt1 + "\n";
                    richTextBox1.Text = richTextBox1.Text + "Начали парсинг... подождите. ";

                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.ScrollToCaret();

                    progressBar1.Value = 0;

                    for (int j = 3; j <= colEndPeriod; j++)
                    {
                        int eventDay = int.Parse(excelApp.Cells[rowPeriod, j].Value.ToString());
                        if (progressBar1.Value < 100) progressBar1.Value = progressBar1.Value + 3;
                        //MessageBox.Show("Day" + eventDay);
                        DateTime newDate = new DateTime(eventYear, eventMonth, eventDay);

                        for (int i = startSmenaFS; i <= endSmenaFS; i++)
                        {

                            if ((excelApp.Cells[i, j].Value != null) && (excelApp.Cells[i, j].Value.ToString() != "0"))
                            {

                                int color = excelApp.Cells[i, j].Interior.ColorIndex;
                                int rrr = 0;
                                string smena = excelApp.Cells[i, j].Value.ToString().Trim();

                                if (int.TryParse(smena, out rrr))
                                {

                                    myEvent newEvent = findEvent(newDate, "" + color);
                                    string dop = "";

                                    if (excelApp.Cells[i, j].Font.Bold)
                                    {

                                        newEvent = findEvent(newDate, "" + 99);
                                        
                                        dop = "(Д)";
                                        if (smena != "12") dop += "(" + smena + ")";

                                        if (newEvent == null)
                                        {
                                            newEvent = new myEvent("FS", "" + 99, allUsers[i - startSmenaFS] + dop, newDate, smenaAll);
                                            if (newEvent.getEventColor() == null) MessageBox.Show("Новый цвет - " + color + "\nКлиент " + allUsers[i - startSmenaFS] + ", дата " + newDate.Day + "число");
                                            allEvents.Add(newEvent);
                                        }
                                        else
                                        {
                                            newEvent.updateEventComment(allUsers[i - startSmenaFS] + dop);
                                        }

                                    }
                                    else
                                    {
                                        if (smena != "12") dop = "(" + smena + ")";
                                        if (newEvent == null)
                                        {
                                            newEvent = new myEvent("FS", "" + color, allUsers[i - startSmenaFS] + dop, newDate, smenaAll);
                                            if (newEvent.getEventColor() == null) MessageBox.Show("Новый цвет - " + color + "\nКлиент " + allUsers[i - startSmenaFS] + ", дата " + newDate.Day + "число");
                                            allEvents.Add(newEvent);
                                        }
                                        else
                                        {
                                            newEvent.updateEventComment(allUsers[i - startSmenaFS] + dop);
                                        }
                                    }
                                }
                            }
                        }
                    }


                }
                catch (Exception ex0)
                {
                    MessageBox.Show("ФС: \n" + ex0.Message);
                }

                progressBar1.Value = 100;

                richTextBox1.Text = richTextBox1.Text + "Парсинг выполнен.";
                

                MemoryStream stream1 = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<myEvent>));

                ser.WriteObject(stream1, allEvents);
                stream1.Position = 0;
                StreamReader sr = new StreamReader(stream1);

                StreamWriter jsonFile = new StreamWriter(filePath + @"_ASFS.json", false, Encoding.Default);
                jsonFile.WriteLine("v0pjjbs5vvid2au7eru8hhabls@group.calendar.google.com");
                jsonFile.WriteLine(dateStartJson);
                jsonFile.WriteLine(dateEndJson);

                jsonFile.Close();

                using (var fileStream = File.Open(filePath + @"_ASFS.json", FileMode.Append))
                {
                    stream1.Position = 0;
                    stream1.WriteTo(fileStream);
                }
                richTextBox1.Text = richTextBox1.Text + " Экспорт JSON произведен.\n\n";

                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();
                //Конец блока АС ФС
                */
               
                
                // Блок выгрузки АС ЕФС
                allUsers = new List<string>();
                allEvents = new List<myEvent>();
                try
                {
                    richTextBox1.Text = richTextBox1.Text + "Собираем график по АС ЕФС...\n";
                    int cnt1 = 0;
                    for (int i = startSmenaEFS; i <= endSmenaEFS; i++)
                    {

                        if (excelApp.Cells[i, 1].Value != null)
                        {

                            richTextBox1.Text = richTextBox1.Text + excelApp.Cells[i, 1].Value + "\n";
                            allUsers.Add(excelApp.Cells[i, 1].Value.Split(' ')[0].Split('-')[0]);
                            cnt1++;
                        }
                        else allUsers.Add("");

                    }
                    richTextBox1.Text = richTextBox1.Text + "Сотрудники в смене: " + cnt1 + "\n";
                    richTextBox1.Text = richTextBox1.Text + "Начали парсинг... подождите. ";

                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.ScrollToCaret();

                    progressBar1.Value = 0;

                    for (int j = 3; j <= colEndPeriod; j++)
                    {
                        int eventDay = int.Parse(excelApp.Cells[rowPeriod, j].Value.ToString());
                        if (progressBar1.Value < 100) progressBar1.Value = progressBar1.Value + 3;
                        //MessageBox.Show("Day" + eventDay);
                        DateTime newDate = new DateTime(eventYear, eventMonth, eventDay);

                        for (int i = startSmenaEFS; i <= endSmenaEFS; i++)
                        {

                            if ((excelApp.Cells[i, j].Value != null) && (excelApp.Cells[i, j].Value.ToString() != "0"))
                            {

                                int color = excelApp.Cells[i, j].Interior.ColorIndex;
                                int rrr = 0;
                                string smena = excelApp.Cells[i, j].Value.ToString().Trim();

                                if (int.TryParse(smena, out rrr))
                                {

                                    myEvent newEvent = findEvent(newDate, "" + color);
                                    string dop = "";

                                    if (excelApp.Cells[i, j].Font.Bold) dop = "(Д)";


                                    if ((smena != "12") && (smena != "13")) dop = "(" + smena + ")";
                                    if (newEvent == null)
                                    {
                                        newEvent = new myEvent("FS", "" + color, allUsers[i - startSmenaEFS] + dop, newDate, smenaEFS);
                                        if (newEvent.getEventColor() == null) MessageBox.Show("Новый цвет - " + color + "\nКлиент " + allUsers[i - startSmenaEFS] + ", дата " + newDate.Day + "число");
                                        allEvents.Add(newEvent);
                                    }
                                    else
                                    {
                                        newEvent.updateEventComment(allUsers[i - startSmenaEFS] + dop);
                                    }

                                }
                            }
                        }
                    }


                }
                catch (Exception ex0)
                {
                    MessageBox.Show("ЕФС: \n" + ex0.Message);
                }

                progressBar1.Value = 100;

                richTextBox1.Text = richTextBox1.Text + "Парсинг выполнен.";


                MemoryStream stream1 = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(List<myEvent>));

                ser.WriteObject(stream1, allEvents);
                stream1.Position = 0;
                StreamReader sr = new StreamReader(stream1);

                StreamWriter jsonFile = new StreamWriter(filePath + @"_EFS.json", false, Encoding.Default);
                jsonFile.WriteLine("0biiqso78lfgfj9blab8ps7vdc@group.calendar.google.com");
                jsonFile.WriteLine(dateStartJson);
                jsonFile.WriteLine(dateEndJson);

                jsonFile.Close();

                using (var fileStream = File.Open(filePath + @"_EFS.json", FileMode.Append))
                {
                    stream1.Position = 0;
                    stream1.WriteTo(fileStream);
                }
                richTextBox1.Text = richTextBox1.Text + " Экспорт JSON произведен.\n\n";

                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();

                //Конец блока АС ЕФС

                // Блок выгрузки ОДС
                allUsers = new List<string>();
                allEvents = new List<myEvent>();
                try
                {
                    richTextBox1.Text = richTextBox1.Text + "Собираем график по ОДС...\n";
                    int cnt1 = 0;
                    for (int i = startSmenaODS; i <= endSmenaODS; i++)
                    {

                        if (excelApp.Cells[i, 1].Value != null)
                        {

                            richTextBox1.Text = richTextBox1.Text + excelApp.Cells[i, 1].Value + "\n";
                            allUsers.Add(excelApp.Cells[i, 1].Value.Split(' ')[0].Split('-')[0]);
                            cnt1++;
                        }
                        else allUsers.Add("");

                    }
                    richTextBox1.Text = richTextBox1.Text + "Сотрудники в смене: " + cnt1 + "\n";
                    richTextBox1.Text = richTextBox1.Text + "Начали парсинг... подождите. ";

                    richTextBox1.SelectionStart = richTextBox1.Text.Length;
                    richTextBox1.ScrollToCaret();

                    progressBar1.Value = 0;

                    for (int j = 3; j <= colEndPeriod; j++)
                    {
                        int eventDay = int.Parse(excelApp.Cells[rowPeriod, j].Value.ToString());
                        if (progressBar1.Value < 100) progressBar1.Value = progressBar1.Value + 3;
                        //MessageBox.Show("Day" + eventDay);
                        DateTime newDate = new DateTime(eventYear, eventMonth, eventDay);

                        for (int i = startSmenaODS; i <= endSmenaODS; i++)
                        {

                            if ((excelApp.Cells[i, j].Value != null) && (excelApp.Cells[i, j].Value.ToString() != "0"))
                            {

                                int color = excelApp.Cells[i, j].Interior.ColorIndex;
                                int rrr = 0;
                                string smena = excelApp.Cells[i, j].Value.ToString().Trim();

                                if (int.TryParse(smena, out rrr))
                                {

                                    myEvent newEvent = findEvent(newDate, "" + color);
                                    string dop = "";

                                    if (excelApp.Cells[i, j].Font.Bold) dop = "(Д)";


                                    if ((smena != "12") && (smena != "13")) dop = "(" + smena + ")";
                                    if (newEvent == null)
                                    {
                                        newEvent = new myEvent("FS", "" + color, allUsers[i - startSmenaODS] + dop, newDate, smenaODS);
                                        if (newEvent.getEventColor() == null) MessageBox.Show("Новый цвет - " + color + "\nКлиент " + allUsers[i - startSmenaODS] + ", дата " + newDate.Day + "число");
                                        allEvents.Add(newEvent);
                                    }
                                    else
                                    {
                                        newEvent.updateEventComment(allUsers[i - startSmenaODS] + dop);
                                    }

                                }
                            }
                        }
                    }


                }
                catch (Exception ex0)
                {
                    MessageBox.Show("ОДС: \n" + ex0.Message);
                }

                progressBar1.Value = 100;

                richTextBox1.Text = richTextBox1.Text + "Парсинг выполнен.";


                stream1 = new MemoryStream();
                ser = new DataContractJsonSerializer(typeof(List<myEvent>));

                ser.WriteObject(stream1, allEvents);
                stream1.Position = 0;
                sr = new StreamReader(stream1);

                jsonFile = new StreamWriter(filePath + @"_ODS.json", false, Encoding.Default);
                jsonFile.WriteLine("bairdkjis3dsjdbbg10e7edua0@group.calendar.google.com");
                jsonFile.WriteLine(dateStartJson);
                jsonFile.WriteLine(dateEndJson);

                jsonFile.Close();

                using (var fileStream = File.Open(filePath + @"_ODS.json", FileMode.Append))
                {
                    stream1.Position = 0;
                    stream1.WriteTo(fileStream);
                }
                richTextBox1.Text = richTextBox1.Text + " Экспорт JSON произведен.\n\n";

                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();

                //Конец блока АС ОДС

            }
            else
            {
                MessageBox.Show("Возникла ошибка, продолжение невозможно.");
            }

            excelApp.ActiveWorkbook.Close();
            excelApp.Quit();

            button2.Enabled = true;


        }




        public string prevZero(int i)
        {
            string res = "" + i;
            if (res.Length == 1) res = "0" + res;
            return res;
        }

        public void bubbleSortTaxi()
        {
            //taxiClass
            taxi elem1 = null;
            taxi elem2 = null;
            int ii = taxiClass.Count - 1;
            for (int i = 0; i < (taxiClass.Count - 1); i++)
            {
                for (int i2 = 0; i2 < ii; i2++)
                {
                    elem1 = taxiClass[i2];
                    elem2 = taxiClass[i2 + 1];

                    DateTime tm1 = getDateFromTaxi(elem1);
                    DateTime tm2 = getDateFromTaxi(elem2);

                    if (tm1 > tm2)
                    {
                        taxiClass[i2] = elem2;
                        taxiClass[i2 + 1] = elem1;
                    }



                }

                ii--;
            }

        }

        private DateTime getDateFromTaxi(taxi tax)
        {
            int day = int.Parse(tax.t2.Split('.')[0]);
            int month = int.Parse(tax.t2.Split('.')[1]);
            int year = int.Parse(tax.t2.Split('.')[2]);

            int hour = int.Parse(tax.t11.Split(':')[0]);
            int minute = int.Parse(tax.t11.Split(':')[1]);

            DateTime result = new DateTime(year, month, day, hour, minute, 0);
            return result;
        }

        public void doXLSXTaxi()
        {
            try
            {
                File.Copy(".\\template.xxl", filePath + "_taxiList.xlsx", true);



                Excel.Application excelApp = new Excel.Application();
                excelApp.Visible = false;

                excelApp.Workbooks.Open(filePath + "_taxiList.xlsx");

                Excel.Worksheet fstWSH = (Excel.Worksheet)excelApp.Workbooks[1].Worksheets[1];


                try
                {
                    progressBar1.Value = 0;
                    for (int i = 0; i < taxiClass.Count - 2; i++)
                    {
                        Excel.Range cellRange = (Excel.Range)excelApp.Cells[17, 1];
                        Excel.Range rowRange = cellRange.EntireRow;
                        rowRange.Insert(Excel.XlInsertShiftDirection.xlShiftDown, false);
                    }
                    progressBar1.Value = 20;

                    int ii = 16;
                    int numPP = 1;
                    foreach (taxi tax in taxiClass)
                    {

                        excelApp.Cells[ii, 2].Value = numPP;
                        excelApp.Cells[ii, 3].Value = tax.t2;
                        excelApp.Cells[ii, 4].Value = tax.t3;
                        excelApp.Cells[ii, 5].Value = tax.t4;
                        excelApp.Cells[ii, 6].Value = tax.t5;
                        excelApp.Cells[ii, 7].Value = tax.t6;
                        excelApp.Cells[ii, 8].Value = tax.t7;
                        excelApp.Cells[ii, 9].Value = tax.t8;
                        excelApp.Cells[ii, 10].Value = tax.t9;
                        excelApp.Cells[ii, 11].Value = tax.t10;
                        excelApp.Cells[ii, 12].Value = tax.t11;
                        excelApp.Cells[ii, 13].Value = tax.t12;
                        //excelApp.Cells[ii, 14].FormulaR1C1 = "=ROUNDUP(MOD(M"+ii+"-L"+ii+";1);1/48)";


                        excelApp.Cells[ii, 14].Formula = "=CEILING(MOD(M" + ii + "-L" + ii + ",1),1/48)";

                        excelApp.Cells[ii, 15].Value = tax.t14;
                        progressBar1.Value = progressBar1.Value + (80 / taxiClass.Count);
                        ii++;
                        numPP++;
                    }


                    //excelApp.Cells[16].PasteSpecial();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

                progressBar1.Value = 100;

                excelApp.ActiveWorkbook.Save();
                excelApp.ActiveWorkbook.Close();
                excelApp.Quit();

                richTextBox1.Text = richTextBox1.Text + "\n Список для такси готов!\n\n";
                richTextBox1.SelectionStart = richTextBox1.Text.Length;
                richTextBox1.ScrollToCaret();

                excelApp.Quit();
                excelApp.Quit();
                excelApp.Quit();

            }
            catch (Exception exEx)
            {
                MessageBox.Show(exEx.Message);
            }

        }



        private myEvent findEvent(DateTime eventDate, string eventType)
        {
            myEvent result = null;



            foreach (myEvent events in allEvents)
            {
                if ((events.getEventDate() == eventDate) && (events.getEventType() == eventType))
                {

                    result = events;
                }
            }

            return result;
        }

        public void doReadCalXML()
        {
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load(".\\config.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlNode xNode in xRoot)
            {
                if (xNode.Name == "smenaEFS")
                {
                    foreach (XmlNode xNode0 in xNode)
                    {
                            smenaEFS.Add(new smena(xNode0.Attributes.GetNamedItem("colorExl").Value,
                            xNode0.Attributes.GetNamedItem("timeStart").Value,
                            xNode0.Attributes.GetNamedItem("timeEnd").Value,
                            xNode0.Attributes.GetNamedItem("colorID").Value,
                            xNode0.Attributes.GetNamedItem("prefix").Value));
                    }

                }
                if (xNode.Name == "smenaODS")
                {
                    foreach (XmlNode xNode0 in xNode)
                    {
                        smenaODS.Add(new smena(xNode0.Attributes.GetNamedItem("colorExl").Value,
                            xNode0.Attributes.GetNamedItem("timeStart").Value,
                            xNode0.Attributes.GetNamedItem("timeEnd").Value,
                            xNode0.Attributes.GetNamedItem("colorID").Value,
                            xNode0.Attributes.GetNamedItem("prefix").Value));
                    }

                }
            }

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form4 fff = new Form4();
            fff.Visible = true;
        }
    }



    [DataContract]
    class myEvent
    {
        [DataMember]
        public myTime start;
        [DataMember]
        public myTime end;

        private string eventType;
        /* 6 Yellow   06:00 - 18:00
         * 3 Red      12:00 - 24:00
         * 14 Green    09:00 - 21:00
         * 33 Blue     21:00 - 09:00
         * 53 Brown    13:00 - 22:00
         * */
        [DataMember]
        private string colorId;
        [DataMember]
        private string summary;

        private DateTime eventDate;

        public myEvent(string sector, string eventType, string eventComment, DateTime eventDate, List<smena> smena)
        {
            this.eventType = eventType;
            this.eventDate = eventDate;
            this.summary = eventComment;
            foreach (smena smn in smena)
            {
                if (smn.colorEx == eventType)
                {
                    this.colorId = smn.colorID;
                    start = new myTime(eventDate.AddHours(smn.timeStart));
                    end = new myTime(eventDate.AddHours(smn.timeEnd));
                    this.summary = smn.prefix + this.summary;
                    break;
                }
            }



        }
        public string getEventType()
        {
            return this.eventType;
        }

        public string getEventColor()
        {
            return this.colorId;
        }

        public DateTime getEventDate()
        {
            return this.eventDate;
        }

        public void updateEventComment(string input)
        {
            this.summary = this.summary + ", " + input;
        }

    }

    [DataContract]
    class myTime
    {
        [DataMember]
        public string dateTime;
        //[DataMember]
        //public string timeZone = "AsiaVladivostok";
        public myTime(DateTime start)
        {
            dateTime = start.ToString("yyyy-MM-ddTH:mm:ss+10:00");
        }
    }


    class taxi
    {
        public string t1, t2, t3, t4, t5, t6, t7, t8, t9, t10, t11, t12, t13, t14;
        public taxi(string t1, string t2, string t3, string t4, string t5, string t6, string t7, string t8, string t9, string t10, string t11, string t12, string t13, string t14)
        {
            this.t1 = t1;
            this.t2 = t2;
            this.t3 = t3;
            this.t4 = t4;
            this.t5 = t5;
            this.t6 = t6;
            this.t7 = t7;
            this.t8 = t8;
            this.t9 = t9;
            this.t10 = t10;
            this.t11 = t11;
            this.t12 = t12;
            this.t13 = t13;
            this.t14 = t14;
        }
    }

    class smena
    {
        public int timeStart, timeEnd;
        public string colorEx, colorID, prefix;
        public smena(string colorEx, string timeStart, string timeEnd, string colorID, string prefix)
        {
            this.colorEx = colorEx;
            this.timeStart = int.Parse(timeStart);
            this.timeEnd = int.Parse(timeEnd);
            this.colorID = colorID;
            this.prefix = prefix;
        }
    }

}

